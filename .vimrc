set nocompatible

syntax enable

set autoindent
set smartindent

set number
set relativenumber

set incsearch
set hlsearch
set smartcase

set tabstop=8
set shiftwidth=8
set expandtab
set softtabstop=8

autocmd FileType make setlocal noexpandtab

set listchars=tab:>-,trail:~,extends:>,precedes:<
set list

noremap <F5> :bp<CR>
noremap <F6> :bn<CR>
noremap <F12> :set number!<CR>:set relativenumber!<CR>

" add this for lightline
set laststatus=2

""" VUNDLE """
" EXAMPLES:
" 'tpope/vim-fugitive' (github repo)
" Plugin 'L9' (from vim-scripts.org/vim/scripts.html)
" Plugin 'git://git.wincent.com/command-t.git' (git, but not hosted on github)
" Plugin 'file:///home/gmarik/path/to/plugin' (local machine)

filetype off  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'w0rp/ale' " Syntac highlighting
        let g:ale_sign_column_always = 1
        let g:ale_c_clang_options = "-Wall -Wextra -Werror"
        let b:ale_linters = ['clang']
        let g:lint_on_enter = 1
        let g:lint_on_save = 1
        let g:lint_on_text_changed = 0
Plugin 'itchyny/lightline.vim' " Change the statusbar
Plugin 'maximbaz/lightline-ale'
Plugin 'taohexxx/lightline-buffer'

        set hidden  " allow buffer switching without saving
        set showtabline=2  " always show tabline

        " use lightline-buffer in lightline
        let g:lightline = {
            \ 'tabline': {
            \   'left': [ [ 'bufferinfo' ],
            \             [ 'separator' ],
            \             [ 'bufferbefore', 'buffercurrent', 'bufferafter' ], ],
            \   'right': [ [ 'close' ], ],
            \ },
            \ 'component_expand': {
            \   'buffercurrent': 'lightline#buffer#buffercurrent',
            \   'bufferbefore': 'lightline#buffer#bufferbefore',
            \   'bufferafter': 'lightline#buffer#bufferafter',
            \ },
            \ 'component_type': {
            \   'buffercurrent': 'tabsel',
            \   'bufferbefore': 'raw',
            \   'bufferafter': 'raw',
            \ },
            \ 'component_function': {
            \   'bufferinfo': 'lightline#buffer#bufferinfo',
            \ },
            \ 'component': {
            \   'separator': '',
            \ },
            \ }

        " remap arrow keys
        nnoremap <Left> :bprev<CR>
        nnoremap <Right> :bnext<CR>

        " lightline-buffer ui settings
        " replace these symbols with ascii characters if your environment does not support unicode
        let g:lightline_buffer_logo = ' '
        let g:lightline_buffer_readonly_icon = ''
        let g:lightline_buffer_modified_icon = '✭'
        let g:lightline_buffer_git_icon = ' '
        let g:lightline_buffer_ellipsis_icon = '..'
        let g:lightline_buffer_expand_left_icon = '◀ '
        let g:lightline_buffer_expand_right_icon = ' ▶'
        let g:lightline_buffer_active_buffer_left_icon = ''
        let g:lightline_buffer_active_buffer_right_icon = ''
        let g:lightline_buffer_separator_icon = '  '

        " enable devicons, only support utf-8
        " require <https://github.com/ryanoasis/vim-devicons>
        let g:lightline_buffer_enable_devicons = 1

        " lightline-buffer function settings
        let g:lightline_buffer_show_bufnr = 1

        " :help filename-modifiers
        let g:lightline_buffer_fname_mod = ':t'

        " hide buffer list
        let g:lightline_buffer_excludes = ['vimfiler']

        " max file name length
        let g:lightline_buffer_maxflen = 30

        " max file extension length
        let g:lightline_buffer_maxfextlen = 3

        " min file name length
        let g:lightline_buffer_minflen = 16

        " min file extension length
        let g:lightline_buffer_minfextlen = 3

        " reserve length for other component (e.g. info, close)
        let g:lightline_buffer_reservelen = 20

Plugin 'scrooloose/nerdcommenter' " Comment tool
    let g:NERDSpaceDelims = 1
    let g:NERDCompactSexyComs = 1

Plugin 'kien/ctrlp.vim'
    let g:ctrlp_map = '<c-p>'
    let g:ctrlp_cmd = 'CtrlP'
    let g:ctrlp_working_path_mode = 'ra'
    let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

