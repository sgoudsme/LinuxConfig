#!/bin/bash

if test -f ~/.bashrc; then
    rm ~/.bashrc
fi
ln .bashrc ~/.bashrc

if test -f ~/.vimrc; then
    rm ~/.vimrc
fi
ln .vimrc ~/.vimrc

if test -f ~/.gitconfig; then
    rm ~/.gitconfig
fi
ln .gitconfig ~/.gitconfig

if test -f ~/.inputrc; then
    rm ~/.inputrc
fi
ln .inputrc ~/.inputrc

# Install VIM
sudo apt install vim


# Install vundle vim plugin
if ! [ -d  ~/.vim/bundle/Vundle.vim ] ; then
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi

# Install fussy finder
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# Install Zoxide
curl -sSfL https://raw.githubusercontent.com/ajeetdsouza/zoxide/main/install.sh | sh

# Install Eza
wget -c https://github.com/eza-community/eza/releases/latest/download/eza_x86_64-unknown-linux-gnu.tar.gz -O - | tar xz
sudo chmod +x eza
sudo chown root:root eza
sudo mv eza /usr/local/bin/eza

# Install Bat
wget -c https://github.com/sharkdp/bat/releases/download/v0.24.0/bat_0.24.0_amd64.deb
sudo dpkg -i bat_0.24.0_amd64.deb  # adapt version number and architecture

# Install tailscale
curl -fsSL https://tailscale.com/install.sh | sh

# Install Lazygit
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
sudo install lazygit /usr/local/bin

echo please log out and back in

