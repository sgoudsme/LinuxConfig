Linux config:
        vimrc
        bachrc
        inputrc
        gdbinit

Attention: change/remove username in .gitconfig

Install (set hardlink):
        ln $(FILE) ~/.$(FILE)

install Vundle:
        git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
